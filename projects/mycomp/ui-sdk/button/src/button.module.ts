import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonComponent } from './button.component';

import { I18nModule } from '@mycomp/ui-sdk/i18n';

@NgModule({
    exports: [ButtonComponent],
    imports: [
        CommonModule,
        I18nModule,
    ],
    declarations: [ButtonComponent]
})
export class ButtonModule { }
